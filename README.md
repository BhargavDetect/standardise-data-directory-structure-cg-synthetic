# Standardise Directory Structure for CG Data

I have prepared a standardized directory structure for synthetic data generation. This will greatly assist in standardizing the directory structure of our synthetic data. It will ensures consistency and ease of use throughout the synthetic data generation pipeline. By adopting this structure, we will create a uniform format that enhances collaboration and simplifies data handling.

I believe that adopting this standardized directory structure will positively impact the overall effectiveness of our projects.

I am open for suggestions. If something needs to be improved then do let me know.

```bash
SyntheticData
│
└───Main_data_class_1
│   └───environment_name_1
│   │   └───sub_environment_name_1
│   │   │   └───object_name_1
│   |   │   │   └───<object_name>_Yes
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   |   │   │   └───<object_name>_No
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   │   │   └───object_name_2
│   │   │   └───object_name_3
│   │   │   └───object_name_n
│   └───environment_name_2
│   │   └───sub_environment_name_1
│   │   │   └───object_name_1
│   |   │   │   └───Yes
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   |   │   │   └───No
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   │   │   └───object_name_2
│   │   │   └───object_name_3
│   │   │   └───object_name_n
└───Main_data_class_2
│   └───IndoorEnvironment
│   │   └───sub_dir_1
│   │   │   └───sub_class_1
│   |   │   │   └───Yes
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   |   │   │   └───No
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   │   │   └───sub_class_2
│   │   │   └───sub_class_3
│   │   │   └───sub_class_n
│   └───OutdoorEnvironment
│   │   └───Sub_dir_1
│   │   │   └───sub_class_1
│   |   │   │   └───Yes
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   |   │   │   └───No
│   |   |   │   │   └───Dataset_<some_id>
│   |   |   │   │   └───InstanceSegmentation_<some_id>
│   |   |   │   │   └───RGB_<some_id>
│   │   └───Sub_dir_2
|   │   └───Sub_dir_n
```

# Example
```bash
SyntheticData
│
└───FireExtinguisher_SynthData
│   │───IndoorEnvironment
|   |   └───WH1
|   │   │   └───FireExtinguisher
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   └───WH2
|   |   └───WH3
│   │───OutdoorEnvironment
|   |   └───Construction
|   │   │   └───FireExtinguisher
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   └───Industry
|   │   └───Port
└───Ladder_SynthData
│   │───IndoorEnvironment
|   |   └───WH1
|   │   │   └───StepLadder~110_WH1
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   │   └───StraightLadder~110_WH1
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   └───WH2
|   |   └───WH3
│
└───PPE_SynthData
│   │───IndoorEnvironment
|   |   └───WH1
|   │   │   └───Facemask
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   │   └───Gloves
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   └───WH2
|   │   └───WH3
|   └───OutdoorEnvironment
|   |   └───Construction
|   │   │   └───Facemask
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   │   └───Gloves
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   └───Industry
|   │   └───Port
└───WheelChoke__SynthData
│   │───IndoorEnvironment
|   |   └───WH1
|   │   │   └───Wheel
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   │   └───Wheel_Choke
|   |   │   │   └───Yes
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   |   │   │   └───No
|   |   |   │   │   └───Dataset_<some_id>
|   |   |   │   │   └───InstanceSegmentation_<some_id>
|   |   |   │   │   └───RGB_<some_id>
|   │   └───WH2
|   │   └───WH3
|   └───OutdoorEnvironment
|   |   └───Construction
|   │   └───Industry
|   │   └───Port
```